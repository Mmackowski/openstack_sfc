#!/usr/bin/python
import pdb

def print_values_server(val, server_id, type):
    if type == 'ports':
        val_list = val['ports']

    if type == 'networks':
        val_list = val['networks']
    for p in val_list:
        bool = False
        for k, v in p.items():
            if k == 'device_id' and v == server_id:
                bool = True
        if bool:
            for k, v in p.items():
                print("%s : %s" % (k, v))

def get_vnfs_id(vnfs,servers):
	print "getting vnfs ids"
	vnfsIds = {}
	for server in servers:
		for vnf in vnfs:
			if vnf == server.name:
				vnfsIds[vnf] = server.id
	return vnfsIds

def get_vnfs_ports_ids(vnfs,vnfsIds,portList):
	print "getting information about neutron ports assigned to vnfs"
	portIds = {}
	for vnf in vnfs:
		portIds[vnf] = []
	for port in portList['ports']:
		for vnf in vnfs:
			if port['device_id'] == vnfsIds[vnf]:
				portData = {}
				portData['id'] = port['id']
				portData['ip'] = port['fixed_ips'][0]['ip_address']
				portData['mac'] = port['mac_address']
				portIds[vnf].append(portData)
	for vnf in vnfs:
		if portIds[vnf][0]['ip'].split('.')[3] > portIds[vnf][1]['ip'].split('.')[3]:
			tmp = portIds[vnf][1]
			portIds[vnf][1] = portIds[vnf][0]
			portIds[vnf][0] = tmp
	return portIds

def get_vnfs_of_ports(vnfs,portIds,sshClient):
	print "fetching openflow port numbers from br-int"
	ofPortsNums = {}
	for vnf in vnfs:
		ofPortsNums[vnf] = []
		for i in range(0,2):
			stdin, stdout, stderr = sshClient.exec_command('ovs-vsctl get Interface qvo'+ portIds[vnf][i]['id'][0:11]+' ofport')
			for line in stdout:
        			ofPortsNums[vnf].append(line.strip('\n'))
	return ofPortsNums

def validate_user_preference(preference,arrayLength):
	value = None
	try:
		value = int(preference)
	except ValueError:
		return -1
	if value > (arrayLength - 1) or value < 0:
		return -1
	return value
	
def get_user_in_out_ports_preferences(vnfs,portIds):
	portPreferences = {}
	vnfsCount = len(vnfs)
	for vnf in range(0,vnfsCount -1):
		vnfNum = vnf + 1
		print "set in and out port preferences for vnf: " + vnfs[vnf] + " " + "{0}".format(vnfNum) + " in chain"
		portCount = len(portIds[vnfs[vnf]])
		for i in range(0,portCount):
			print "choice {0}: mac - ".format(i) + portIds[vnfs[vnf]][i]['mac'] + " ip - " + portIds[vnfs[vnf]][i]['ip'] 
		
		inPort = None
		outPort = None
		if vnf != 0:
			while True:
				print "choose vnf in port:"
				inPort = validate_user_preference(raw_input(),portCount)
				if inPort!=-1:
					break;
				else: 
					print "wrong value"
		if vnf != (vnfsCount - 1):
			while True:
				print "choose vnf out port:"
				outPort = validate_user_preference(raw_input(),portCount)
				if outPort!=-1:
					break;
				else:
					print "wrong value"
		portPreferences[vnfs[vnf]] = []
		portPreferences[vnfs[vnf]].append(inPort)
		portPreferences[vnfs[vnf]].append(outPort)
	return portPreferences
			
def set_sfc_openflow_rules(vnfs,portIds,ofPortNums,sshClient):
	print "setting opentflow rules"
	vnfscount = len(vnfs)
	#pdb.set_trace()
  	#print "setting rule to forward matching traffic from  first vnf to secound"
	stdin, stdout, stderr = sshClient.exec_command("ovs-ofctl add-flow br-int in_port=" + 
							ofPortNums[vnfs[0]][0] + ",dl_dst=" + 
							portIds[vnfs[vnfscount-1]][0]['mac'] + 
							',actions=output:' + ofPortNums[vnfs[1]][0])
	for line in stderr:
		print line

	for i in range(1,vnfscount-1):
		stdin, stdout, stderr = sshClient.exec_command("ovs-ofctl add-flow br-int in_port=" +
					ofPortNums[vnfs[i]][1] + ",dl_dst=" +
					portIds[vnfs[vnfscount-1]][0]['mac'] +
					',actions=output:' + ofPortNums[vnfs[i+1]][0])
		for line in stderr:
			print line
	print ":p"
