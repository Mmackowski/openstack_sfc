#!/usr/bin/python

import os,sfcUtils, paramiko
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client
from novaclient import client as nvclient
from neutronclient.v2_0 import client as neutronclient


#getting vnfs names
vnfs = os.environ['CHAIN_VNFS'].split(',')

#creating keystone authorization session
auth = v3.Password(auth_url=os.environ['OS_AUTH_URL'],
                    username=os.environ['OS_USERNAME'],
                    password=os.environ['OS_PASSWORD'],
                    project_name=os.environ['OS_PROJECT_NAME'],
                    project_domain_name=os.environ['OS_PROJECT_DOMAIN_NAME'],
                    user_domain_name=os.environ['OS_USER_DOMAIN_NAME'])
sess = session.Session(auth=auth)

#getting novaclient instance
nova = nvclient.Client(2,session=sess)
servers = nova.servers.list()

#initiate neutron client instance
neutron = neutronclient.Client(session=sess)

#getting vnfs ids
vnfsIds = sfcUtils.get_vnfs_id(vnfs,servers)

#getting openstack ids of neutorn ports 
portIds = sfcUtils.get_vnfs_ports_ids(vnfs,vnfsIds,neutron.list_ports())

sshClient = paramiko.SSHClient()
policy = paramiko.client.AutoAddPolicy()
sshClient.set_missing_host_key_policy(policy)
sshClient.connect(os.environ['SFC_HOSTNAME'],
		username=os.environ['SFC_HOST_USERNAME'],
		password=os.environ['SFC_USER_PASSWORD'])

ofPortsNums = sfcUtils.get_vnfs_of_ports(vnfs,portIds,sshClient)
sfcUtils.set_sfc_openflow_rules(vnfs,portIds,ofPortsNums,sshClient)
sshClient.close()

#getting openflow ports 




